﻿using EmployeeManagement.DAL;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;

namespace EmployeeManagement.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        [HttpGet]
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            var dtEmployee = new DataTable();
            var client = new HttpClient();

            client.DefaultRequestHeaders.Add("User-Agent", "C# console program");
            client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

            var url = "http://dummy.restapiexample.com/api/v1/employees";
            System.Threading.Thread.Sleep(5000);
            HttpResponseMessage response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var resp = await response.Content.ReadAsStringAsync();
            var result = JObject.Parse(resp);
            var items = result["data"].Children().ToList();

            dtEmployee.Columns.Add("id");
            dtEmployee.Columns.Add("employee_name");
            dtEmployee.Columns.Add("employee_salary");
            dtEmployee.Columns.Add("employee_age");
            dtEmployee.Columns.Add("profile_image");
            dtEmployee.Columns.Add("average_age");
            dtEmployee.Columns.Add("average_salary");
            foreach (var subItem in items)
            {
                dtEmployee.Rows.Add(new object[] { subItem["id"], subItem["employee_name"], subItem["employee_salary"], subItem["employee_age"], subItem["profile_image"] });
            }
            int AverageAge = decimal.ToInt32(dtEmployee.Select().Where(p => p["employee_age"] != null).Select(c => Convert.ToDecimal(c["employee_age"])).Average());
            decimal AverageSalary = Math.Round(dtEmployee.Select().Where(p => p["employee_salary"] != null).Select(c => Convert.ToDecimal(c["employee_salary"])).Average(), 2);
            foreach (DataRow row in dtEmployee.Rows)
            {
                if (Convert.ToDecimal(row["employee_salary"]) > AverageSalary)
                {
                    row["average_salary"] = "Above";
                }
                else if (Convert.ToDecimal(row["employee_salary"]) < AverageSalary)
                {
                    row["average_salary"] = "Below";
                }
                else
                {
                    row["average_salary"] = "Equal";
                }
                if (Convert.ToInt32(row["employee_age"]) > AverageAge)
                {
                    row["average_age"] = "Above";
                }
                else if (Convert.ToInt32(row["employee_age"]) < AverageAge)
                {
                    row["average_age"] = "Below";
                }
                else
                {
                    row["average_age"] = "Equal";
                }
            }
            ViewBag.AverageAge = AverageAge;
            ViewBag.AverageSalary = AverageSalary;
            ViewBag.dtEmployee = dtEmployee;

            return View();
        }


        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
